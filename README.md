# Secpass

## _A minimal, security-focused password manager_

**Secpass** is a simple Python script which serves as a **minimal**, **security-focused** password manager alternatives for those looking to store passwords locally with the utmost security and stealth.

## Features
- **Secpass** lack of features is the main feature.
- No internet connection is ever used.
- Password database does not contain a file signature, and if deleted would be unrecoverable by forensics tools.
- Minimal interface
- Under **350** lines of code


## Installation & Usage
You must have Python 3 installed. Follow your operating system instructions.

You then must install *requirements.txt* via running the following terminal command: 
```sh
python3 -m pip -r requirements.txt
```

You can run **Secpass** via issuing the following terminal command:
```sh
python3 secpass.py
```

## License
This project is released under the **MIT** license which can be found at [*/license*](https://codeberg.org/Light-Project/secpass/src/branch/main/LICENSE)